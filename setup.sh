#!/bin/bash

export MODE="overwrite" #"skip_exist";

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase # use your path
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

# More memory
ulimit -S -s unlimited

export basedir=$PWD
export PATH=${basedir}/python:${basedir}/scripts:${basedir}/submodules/quickstats/bin:$PATH
export PYTHONPATH=${basedir}/python/:${basedir}/scripts/:${basedir}/submodules/quickstats:$PYTHONPATH

if [[ -z $1 ]] || [[ $1 == 'compile' ]]; then # The default
    #echo 'setup LCG_103, ROOT 6.28/00'
    #lsetup "views LCG_103 x86_64-centos7-gcc11-opt"
    echo 'setup LCG_102b, ROOT 6.26/08'
    lsetup "views LCG_102b x86_64-centos7-gcc11-opt"
    if [[ $1 == 'compile' ]]; then # First time to compile quickstats
        quickstats compile
    fi
elif [[ $1 == '103' ]]; then
    echo 'setup LCG_103, ROOT 6.28/00'
    lsetup "views LCG_103 x86_64-centos7-gcc11-opt"
    if [[ $2 == 'compile' ]]; then
        quickstats compile
    fi
elif [[ $1 == 'Stat' ]]; then
    echo 'setup StatAnalysis'
    asetup StatAnalysis,0.2.0
    return
else
    return
fi
